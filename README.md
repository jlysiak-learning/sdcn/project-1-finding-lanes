# **Finding Lane Lines on the Road** 

[![Udacity - Self-Driving Car NanoDegree](https://s3.amazonaws.com/udacity-sdc/github/shield-carnd.svg)](http://www.udacity.com/drive)

Very simple pipeline for finding right and left lanes on the road.

Example:

<img src="test_images_output/whiteCarLaneSwitch.jpg" width="480" alt="Combined Image" />

