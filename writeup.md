# **Finding Lane Lines on the Road** 
---

**Finding Lane Lines on the Road**

The goals / steps of this project are the following:
* Make a pipeline that finds lane lines on the road
* Reflect on your work in a written report

[std]: ./test_images/whiteCarLaneSwitch.jpg
[lines]: ./test_images_output/whiteCarLaneSwitch.jpg
[lanes]: ./test_images_output_lanes/whiteCarLaneSwitch.jpg
---
### Pipeline description

The pipeline code is in `get_lines` and `find_lanes` functions. Params are grouped in `Params` class. 
`find_lanes` uses also low-pass filter, which object is passed as argument (more about the filter below).

#### Drawing lines
The steps to obtain, from such a picture ![std][std] the picture like this ![lines][lines] is as followos:

1. Generate ROI vertices using params
2. Use Gamma correction on full RGB image to obtain slightly better contrast
4. Find edges using Canny algorithm
    - Convert full RGB image to grayscale
    - Apply gaussiain blur
    - Apply Canny edge detection using params from Params class
5. Apply ROI to selet only interesting edges
6. Apply Hough transform to find lines
7. Draw lines

#### Finding lanes
To obtain the picture like this ![lanes][lanes] the following steps has done:

1. Get lines from the previous section
7. Detect left and right lane
    - Iterate over all lines obtained in the previous step
    - Calculate the angle of the lane in the picture
    - Depending on the angle move is to left or right set of lines
8. Fit linear models to left and rigth set of points, independently
9. Apply low-pass filter on `m` and `b` parameter for left and right lane
10. Use filtered values to draw lanes.
11. Highligh the ROI on the RGB image to see what we are taking into account.

### Potential shortcomings with the current pipeline
1. I've not spent so much time on fine-tuning of the parameters used, therefore sometimes line detetctor may vary, but low-pass filter does a good job.
2. I haven't use color selector. Good params for yellow and white may be really helpful in tacking video 3.

### Possible improvements

1. Make thresholds adaptive. It could help in the video 3, where the road colors are chaning and it's hard to detect the yellow lane.
2. Better parameter tuning
